﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WikeSoft.Core.Extension;

namespace WikeSoft.Core
{
    /// <summary>
    /// 枚举工具
    /// </summary>
    public static class EnumUtility
    {
        public static IEnumerable<SelectListItem> GetValuesWithNullableValue(Type enumType,string allText, params object[] exceptValues)
        {
            yield return new SelectListItem { Text = allText, Value = "" };
            var values = GetValues(enumType, exceptValues);
            foreach (var value in values)
            {
                yield return value;
            }
        }


        /// <summary>
        /// GetValues
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="exceptValues"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> GetValues(Type enumType, params object[] exceptValues)
        {
            var values = Enum.GetValues(enumType);
            return SelectValue(values, exceptValues);
        }

        /// <summary>
        /// GetSelectOptions
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="exceptValues"></param>
        /// <returns></returns>
        public static IEnumerable<SelectOption> GetSelectOptions(Type enumType, params object[] exceptValues)
        {
            var values = Enum.GetValues(enumType);
            return SelectOption(values, exceptValues);
        }

        /// <summary>
        /// SelectValue
        /// </summary>
        /// <param name="values"></param>
        /// <param name="exceptValues"></param>
        /// <returns></returns>
        private static IEnumerable<SelectListItem> SelectValue(Array values, params object[] exceptValues)
        {
            return from object value in values
                   where exceptValues != null && !exceptValues.Contains(value)
                   select new SelectListItem
                   {
                       Text = value.GetDescriptionForEnum(),
                       Value = value.ToString()
                   };
        }

        /// <summary>
        /// SelectOption
        /// </summary>
        /// <param name="values"></param>
        /// <param name="exceptValues"></param>
        /// <returns></returns>
        private static IEnumerable<SelectOption> SelectOption(Array values, params object[] exceptValues)
        {
            return from object value in values
                   where exceptValues != null && !exceptValues.Contains(value)
                   select new SelectOption
                   {
                       Text = value.GetDescriptionForEnum(),
                       Value = ((int)value).ToString()
                   };
        }

        /// <summary>
        /// GetValues
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exceptValues"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> GetValues<T>(params object[] exceptValues)
        {
            var values = Enum.GetValues(typeof(T));
            return SelectValue(values, exceptValues);
        }

        /// <summary>
        /// GetNullableValues
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exceptValues"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> GetNullableValues<T>(params object[] exceptValues)
        {
            yield return new SelectListItem { Text = "所有", Value = "" };

            foreach (var selectListItem in GetValues<T>(exceptValues))
            {
                yield return selectListItem;
            }   
        } 
    }
}
