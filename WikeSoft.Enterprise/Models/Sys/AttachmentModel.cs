﻿using System;
using System.ComponentModel.DataAnnotations;
using WikeSoft.Data.Models;

namespace WikeSoft.Enterprise.Models.Sys
{
    /// <summary>
    /// 附件模型
    /// </summary>
    public class AttachmentModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 是否为图片
        ///</summary>
        public bool IsImg { get; set; } // IsImg

        ///<summary>
        /// 完整的URL(http://image.wikesoft.cn/aaa.jpg)
        ///</summary>
        public string UrlPath { get; set; } // UrlPath (length: 500)

        ///<summary>
        /// 略缩图完整的URL(http://image.wikesoft.cn/aaa.jpg)
        ///</summary>
        public string ThumUrlPath { get; set; } // ThumUrlPath (length: 500)

        ///<summary>
        /// 真实的名称
        ///</summary>
        public string TrueName { get; set; } // TrueName (length: 500)

        ///<summary>
        /// HttpConentType
        ///</summary>
        public string ContentType { get; set; } // ContentType (length: 500)

        ///<summary>
        /// 创建时间
        ///</summary>
        public DateTime? CreateDate { get; set; }
    }

    /// <summary>
    /// 附件添加模型
    /// </summary>
    public class AttachmentAddModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)
        ///<summary>
        /// 是否为图片
        ///</summary>
        [Display(Name = "是否为图片")]
        public bool IsImg { get; set; } // IsImg

        ///<summary>
        /// 完整的URL(http://image.wikesoft.cn/aaa.jpg)
        ///</summary>
        [Display(Name = "URL地址")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string UrlPath { get; set; } // UrlPath (length: 500)

        ///<summary>
        /// 略缩图完整的URL(http://image.wikesoft.cn/aaa.jpg)
        ///</summary>
        [Display(Name = "略缩图URL")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string ThumUrlPath { get; set; } // ThumUrlPath (length: 500)

        ///<summary>
        /// 真实的名称
        ///</summary>
        [Display(Name = "图片名称")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string TrueName { get; set; } // TrueName (length: 500)

        ///<summary>
        /// HttpConentType
        ///</summary>
        [Display(Name = "图片类型")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string ContentType { get; set; } // ContentType (length: 500)
    }
}
