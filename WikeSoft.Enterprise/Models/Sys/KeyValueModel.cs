﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.Enterprise.Models;

namespace WikeSoft.Data.Models.Sys
{
    public class KeyValueModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 中文名称
        ///</summary>
        public string CnName { get; set; } // CnName (length: 500)

        ///<summary>
        /// 键
        ///</summary>
        public string Key { get; set; } // Key (length: 500)

        ///<summary>
        /// 值
        ///</summary>
        public string Value { get; set; } // Value (length: 500)

        ///<summary>
        /// 说明
        ///</summary>
        public string Memo { get; set; } // Memo (length: 500)
    }

    public class KeyValueAddModel
    {
        ///<summary>
        /// 中文名称
        ///</summary>
        [Display(Name = "中文名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string CnName { get; set; } // CnName (length: 500)

        ///<summary>
        /// 键
        ///</summary>
        [Display(Name = "键")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string Key { get; set; } // Key (length: 500)

        ///<summary>
        /// 值
        ///</summary>
        [Display(Name = "值")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string Value { get; set; } // Value (length: 500)

        ///<summary>
        /// 说明
        ///</summary>
        [Display(Name = "说明")]
        
        public string Memo { get; set; } // Memo (length: 500)
    }

    public class KeyValueEditModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 中文名称
        ///</summary>
        [Display(Name = "中文名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string CnName { get; set; } // CnName (length: 500)

        ///<summary>
        /// 键
        ///</summary>
        [Display(Name = "键")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string Key { get; set; } // Key (length: 500)

        ///<summary>
        /// 值
        ///</summary>
        [Display(Name = "值")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        public string Value { get; set; } // Value (length: 500)

        ///<summary>
        /// 说明
        ///</summary>
        [Display(Name = "说明")]

        public string Memo { get; set; } // Memo (length: 500)
    }
}
