﻿$(function() {
    $.get("/PowerDecision/GetStatPieDatas", function (datas) {
        // 路径配置
        require.config({
            paths: {
                echarts: '/Scripts/echarts.2.2.7/build/dist'
            }
        });
        // 使用
        require(
            [
                'echarts',
                'echarts/chart/bar', // 使用柱状图就加载bar模块，按需加载
                'echarts/chart/pie' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                var myChart1 = ec.init(document.getElementById('pieMain'));

                var option1 = {
                    title: {
                        text: '饼状图分析：有问题数量所占比例',

                        x: 'center'
                    },
                    tooltip: {
                        trigger: 'item',
                        position: "outer",
                        show:true,

                        formatter: "{a} {b} : {c} ({d}%)"
                    },
                    calculable: true,
                    series: [
                        {
                            type: 'pie',
                            radius: '55%',
                            center: ['50%', '60%'],
                            itemStyle:{
                                normal:{
                                    label:{
                                        show: true,
                                        formatter: '{b} : {c} ({d}%)',
                                        textStyle: {
                                            fontSize: 20
                                        }
                                    },
                                    labelLine :{show:true}
                                }
                            },
                            data: datas
                        }
                    ]
                };
                // 为echarts对象加载数据
                myChart1.setOption(option1);
            }
        );
    });
});