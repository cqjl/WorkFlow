﻿function openPostDialog() {
    top.layer.open({
        title: '功能查询',
        type: 2,
        content: "/Department/ListView",
        area: ['1000px', '550px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var tem = $(layero).find("iframe")[0].contentWindow.getContent();
            setParentData(tem.Id, tem.DepartmentName);
            top.layer.close(index);
        }, cancel: function () {
            return true;
        }
    });
}
function setParentData(id, departmentName) {
   
    $("#ParentId").val(id);
    $("#ParentName").val(departmentName);
}


$(document).ready(function () {
    $("#btnSelectPost").click(openPostDialog);


    $("#btnClear").click(function () {
        //因为ParentId要求不能为空，设置成空的GUID，则为根节点
        $("#ParentId").val("00000000-0000-0000-0000-000000000000");
        $("#ParentName").val("");
    });


});