﻿function getDutyConfig(dp) {
    var dutyyear = dp.cal.getNewDateStr();
    getDutyConfigByYear(dutyyear);
}

function getDutyConfigByYear(dutyyear) {
    $.ajax({
        type: "get",
        url: "/DutyConfig/GetDutyConfig",
        contentType: "application/json, charset=utf-8",
        datatype: 'JSON',
        data: { year: dutyyear },
        success: function (res) {
            createconfig(res.data);
        }
    });
}

function createconfig(list) {
    var html = "<option value=''>责任名称</option>";
    for (var i = 0; i < list.length; i++) {
        html = html + "<option value='" + list[i].Id + "'>" + list[i].DutyName + "</option>";
    }
    $("#DutyConfigId").html(html);
    $("#DutyConfigDetailId").html("<option value=''>责任明细</option>");
}
function getDutyConfigDetail(dutyconfigId) {
    $.ajax({
        type: "get",
        url: "/DutyConfig/GetDutyConfigDetail",
        contentType: "application/json, charset=utf-8",
        datatype: 'JSON',
        data: { DutyConfigId: dutyconfigId },
        success: function (ret) {
            createconfigdetail(ret.data);
        }
    });
}

function setDutyStatus(val) {
    $("#DutyStatus").val(val);
    return true;
}



function createconfigdetail(list) {
    var html = "<option value=''>责任明细</option>";
    for (var i = 0; i < list.length; i++) {
        html = html + "<option value='" + list[i].Id + "'>" + list[i].DutyDetail + "</option>";
    }
    $("#DutyConfigDetailId").html(html);
}

function setPostData(postId, postName) {

    $("#PostId").val(postId);
    $("#PostName").val(postName);
};



$(function () {
    $("#btnClearPost").click(function () {

        $("#PostId").val("");
        $("#PostName").val("");
    });

    $("#btnSelectPost").click(function () {

        parent.layer.open({
            title: '岗位查询',
            type: 2,
            content: "/Post/QueryPosts",
            area: ['1000px', '550px'],
            btn: ['确认', '关闭'],
            btnclass: ['btn btn-primary', 'btn btn-danger'],
            yes: function (index, layero) {
                var tem = $(layero).find("iframe")[0].contentWindow.getContent();
                setPostData(tem.Id, tem.PostName);
                parent.layer.close(index);
            },
            cancel: function () {
                return true;
            }
        });
    });

    $("#DutyConfigId").change(function () {

        if ($(this).val() !== "") {

            var config = $(this).val();
            getDutyConfigDetail(config);

        } else {
            $("#DutyConfigDetailId").html("<option value=''>责任明细</option>");
        }
    });

    $("#btnEditFinish").click(function() {
        
        var row = WikeGrid.GetData();
        if (row != null) {
            $(this).button("loading");
            window.location.href = $(this).data("url") + "/" + row.Id;
        } else {
            parent.layer.alert("请选择要操作的数据");
        }
    });

    $("#btnShowResult").click(function() {
        var row = WikeGrid.GetData();
        if (row != null) {
            $(this).button("loading");
            window.location.href = $(this).data("url") + "/" + row.Id;
        } else {
            parent.layer.alert("请选择要操作的数据");
        }
    });
});