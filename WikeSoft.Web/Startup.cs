﻿using Microsoft.Owin;
using Owin;
using WikeSoft.Web;

[assembly: OwinStartup(typeof(Startup))]
namespace WikeSoft.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
