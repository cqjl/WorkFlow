﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class TaskDefinition
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // NodeDefId (Primary key)

        ///<summary>
        /// 外键，流程定义ID
        ///</summary>
        public string FlowDefId { get; set; } // FlowDefId

        ///<summary>
        /// 节点名称
        ///</summary>
        public string NodeDefName { get; set; } // NodeDefName (length: 50)

        ///<summary>
        /// 节点位置 ，左边距
        ///</summary>
        public int LeftX { get; set; } // LeftX

        ///<summary>
        /// 节点位置 ，上边距
        ///</summary>
        public int TopX { get; set; } // TopX

        ///<summary>
        /// 节点宽度
        ///</summary>
        public int Width { get; set; } // Width

        ///<summary>
        /// 节点高度
        ///</summary>
        public int Height { get; set; } // Height

        ///<summary>
        /// 节点类型（start:开始，task:任务,end:结束）
        ///</summary>
        public string NodeType { get; set; } // NodeType (length: 50)

        public string RoleNames { get; set; }

        public string RoleIds { get; set; }

        public string UserNames { get; set; }

        public string UserIds { get; set; }

        public TaskDefinition()
        {
            RoleNames = string.Empty;
            RoleIds = string.Empty;
            UserNames = string.Empty;
            UserIds = string.Empty;
        }
    }
}
